/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: pawn
 *
 * Created on September 29, 2017, 10:02 PM
 */

#include <vector>
#include <map>
#include <set>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <iomanip>
/*
 * 
 */
void readAsker();
void queryPrinter(const std::string, std::vector<std::string> &, std::map<std::string, std::set<int>> &);
void fileLoader(const std::string, std::vector<std::string>&, std::map<std::string, std::set<int>> &);
std::ifstream &vectorLoader(std::ifstream &, std::vector<std::string>&);
void mapLoader(std::vector<std::string> &, std::map<std::string,std::set<int>> &); 

//read a file in and allow the user to perform the query

int main(int argc, char** argv) {

    std::ifstream inFile;
    std::string fileName;
    std::string queryTerm;
    unsigned int selection = 0;
    std::vector<std::string> textLines;
    std::map<std::string, std::set<int>> wordPositions;

    std::cout << "What do you wish to do?\n"
            << "***********************************************\n"
            << "1. Perform a Query.\n"
            << "2. Load Data\n"
            << "3. Exit\n";
    while (std::cin >> selection && selection != 3) {
        //query for the file name, must make sure its valid
        //load it into the vector
        switch (selection) {
            case 1://case 1 we have entered query time
                std::cout << "Perform a Query Selected!"
                        << "Please enter the query term: ";
                std::cin >> queryTerm;
                queryPrinter(queryTerm, textLines, wordPositions);
                break;
            case 2://case 2 we want to load more data
                std::cout << "*********************************************\n";
                std::cout << "Please enter a new or valid file name: ";
                std::cin >> fileName;
                //load the file into the system
                if (!fileName.empty())
                    fileLoader(fileName, textLines, wordPositions);
                break;
            case 3://exit the program
                std::cout << "***********************************************\n"
                        << "Exiting! Thank you!";
                break;
            case 4://display prompt to continue
                std::cout << "What do you wish to do?\n"
                        << "***********************************************\n"
                        << "1. Perform a Query.\n"
                        << "2. Load Data\n"
                        << "3. Exit\n";
                break;
            case 5:
                std::cout << "Please enter the file name of the file you wish to query for content: ";
                break;
        }
        

        std::cout << "What do you wish to do?\n"
                << "***********************************************\n"
                << "1. Perform a Query\n"
                << "2. Load Data\n"
                << "3. Exit\n";
    }

    return 0;
}

//function that does the work of loading things into the containers

void fileLoader(const std::string fileName, std::vector<std::string> &textLines, std::map<std::string, std::set<int>> &wordPositions) {
    std::ifstream in(fileName);
    vectorLoader(in, textLines);

    std::cout << "Current content in the system: \n";
    //display the contents of the vector textLines to make sure we have a good read
    for (auto i : textLines)
        std::cout << i << "\n";
    
    //here is where we call map loader to find the data
    mapLoader(textLines, wordPositions);
    
    std::cout << "Content finished the read\n";

    //now interate over the vector and find the words and their locations
    //and put them into a map<string, set<int>>
}

//function that will load the contents into a vector
std::ifstream& vectorLoader(std::ifstream &inFile, std::vector<std::string> &textLines) {
    std::string currLine;
    //read that file stream into the vector for lines of text
    //read while everything is good and no error flags are set
    while (getline(inFile, currLine) && inFile.good())
        textLines.push_back(currLine);

    return inFile;
}

//function to find the positions of each word, and then find the locations of said word and load them into the map

void mapLoader(std::vector<std::string> &textLines, std::map<std::string, std::set<int>> &wordPositions) {
    std::string currLine;
    std::string currWord;
    int lineNum = 0;//this is set to zero so that we can select from both the map and the vector using the same number
    //on display to the user this should be +1'd

    //for each line in textLines
    for (std::string line : textLines) {
        
        //make a stringstream from each line
        std::istringstream inLine(line);
        
        //take the line and extract every word
        while (inLine >> currWord && inLine.good()){
            
            //if the word isnt already there, insert it
            if(wordPositions.find(currWord) == wordPositions.end())
                wordPositions.insert({currWord,{lineNum}});
            else//if the word is there insert the lineNum
                wordPositions.at(currWord).insert(lineNum);
        }
        
        ++lineNum;
    }
}

//function to print the query content
void queryPrinter(const std::string searchTerm, std::vector<std::string> &textLines, std::map<std::string, std::set<int>> &wordPositions){
    int count = 0;
    
    //if the word is present
    if(wordPositions.find(searchTerm) != wordPositions.end()){
        
        //calculate the size of the set containing the lines
        count = wordPositions.at(searchTerm).size();
        std::cout << "Search Term found: " << count << " Time(s)\n";
        std::cout << "Lines the term appears on: \n";
        
        //display the lines containing the text we need, we +1 lineIter because we had it starting at zero due to vector positions
        for(auto lineIter = wordPositions.at(searchTerm).cbegin(); lineIter != wordPositions.at(searchTerm).cend(); ++lineIter){
            std::cout<< "Line "<< std::setw(2) << (*lineIter)+1 << ": " << textLines[*lineIter] << "\n";
        }
    }
    else//tell the user that the term was not found
        std::cout << "Search Term: " << searchTerm << " NOT FOUND!";
    
    
}
